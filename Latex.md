\documentclass[10pt, oneside]{article}
\usepackage[a4paper, left=15mm,top=20mm, right=15mm, bottom=20mm]{geometry}
\newcommand{\student}{Nanzhi FANG}%Enter here your name
\newcommand{\email}{nanzhi.fang@etu.univ-grenoble-alpes.fr}%Enter here your email
\newcommand{\homeworknumber}{\#}%Enter the homework number
\usepackage{framed}
\usepackage{mathtools}
\usepackage[utf8]{inputenc} 
\usepackage[french]{babel}
\usepackage{longtable}
\usepackage{tocloft}
\renewcommand{\cftsecleader}{\cftdotfill{\cftdotsep}}
\newcommand{\UGA}{Universit\'e Grenoble Alpes}
\newcommand{\PHITEM}{UFR PhITEM}
\usepackage{enumitem}
\usepackage{xspace}
\usepackage{xcolor}
\usepackage{hyperref}
\hypersetup{
pdfencoding=auto,
    colorlinks,
    linkcolor={red!50!black},
    citecolor={blue!50!black},
    urlcolor={blue!80!black}}
\usepackage[T1]{fontenc}
\usepackage[hyphenbreaks]{breakurl}
\author{\student}
\colorlet{shadecolor}{gray!25}
\renewenvironment{leftbar}{%
   \def\FrameCommand{\textcolor{shadecolor}{\vrule width 2pt} \hspace{10pt}}%
   \MakeFramed {\advance\hsize-\width \FrameRestore}}%
{\endMakeFramed}
%\allsectionsfont{\centering} 
%%%%%%%
\usepackage{titlesec}

\makeatletter
\newif\if@secnum
\definecolor{lgray}{gray}{0.75}
\titleformat{\section}[hang]{\Large\bfseries}{%
    \global\@secnumtrue
}{0em}{%
    {%
        \setlength{\fboxsep}{0pt}%
        \colorbox{lgray}{\makebox[\textwidth]{\Large\strut}}%
    }%
    \hspace*{-\textwidth}%
    \if@secnum%
        \arabic{section}%
        \hspace*{1em}%
    \fi%
    \global\@secnumfalse%
}[]
\makeatother
\usepackage{graphicx}
\usepackage{subfigure}
%\usepackage{caption}
\usepackage[]{caption2}
\usepackage{subcaption}
\usepackage{float}
%%%%%%%
\begin{document}
\pagenumbering{gobble}
\begin{titlepage}
\begin{center}
{\Huge{\bf \UGA}}\\
\vskip0.5cm
\includegraphics[scale=0.2]{template/Image/Logo_Université_Grenoble_Alpes_2020.svg.png}
\vskip1cm
{\LARGE{\bf \PHITEM}}
\vskip1cm
{\Large{\bf Licence EEA}}
\end{center}
\noindent\makebox[\linewidth]{\rule{0.5\paperwidth}{0.8pt}}
\vskip1cm
\begin{center}
\par
{\Large\textbf{Signaux et Systèmes}}

\vspace{0.5cm}
{\Large\textbf{TP : Filtrage par circuits commutés}}
\vskip0.5cm
\today

\end{center}

\noindent\makebox[\linewidth]{\rule{0.8\paperwidth}{2pt}}
\vspace{0.2cm}

\begin{longtable}[l]{p{150pt} p{273pt}} 
Name:&\student\\
Email address:&\href{mailto:\email}{\email}
\end{longtable}
\noindent\makebox[\linewidth]{\rule{0.8\paperwidth}{0.8pt}}
\setcounter{tocdepth}{1}
\tableofcontents 
\end{titlepage}
%\includepdfmerge{annexes/galaxie1.eps,annexes/galaxie2.eps,annexes/galaxie3.eps}
\leavevmode\thispagestyle{empty}\newpage
\pagenumbering{arabic}
\section{Introduction}
Dans cette séance de travaux pratique, nous allons étudier les signaux filtrés par des circuits à commutation via la maquette ci-dessus(la Figure 1). Nous pouvons considérer ces signaux comme des signaux analogiques ayant subi un échantillonnage et une opération de maintien ou blocage. Nous analysons leurs caractéristiques temporelle et fréquentielle à l'aide d'un oscilloscope numérique qui à son tour effectue un échantillonnage et une numérisation permettant d'afficher soit la représentation temporelle, soit la représentation fréquentielle sous la forme du spectre FFT ("fast Fourier transform").
\begin{figure}[!h]
\centering
\subfigure[pratique]{
\label{Fig.sub.1}
\includegraphics[scale=0.09]{template/Image/CarteFiltre.jpg}}
\subfigure[théorique]{
\label{Fig.sub.2}
\includegraphics[scale=0.45]{template/Image/Maquette.png}}
\label{Fig.main}
\caption{Maquette utilisé}
\end{figure}\par
La maquette a deux filtres passe-bas à capacités commutées de type LTC1064, et leur horloge de commande peut être choisie soit commune avec l'horloge interne, soit externe(signal TTL à mettre à l'entrée horloge n°2) en actionnant le bouton inverseur situé au centre de la maquette. Nous allons présenter le sujet dans trois parties dans l'ordre : la préparation, les mesures et la conclusion.
\section{préparation}
\textbf{Question} : D'après les courbes de réponse données en annexe dans les documentations, retracer les modules des transmittances en fonction du rapport $f/f_c$(où fc = 95[kHz] pour le LTC1064-3 et fc = 100[kHz], courbe C pour le LTC1064-4) en échelle linéaire sur papier millimétré.\newline 
\textbf{Réponse} : 
\begin{figure}[!ht]\centering
\includegraphics[scale=0.8]{template/Image/capcom2.png}
\caption{Module de la transmittance du ﬁltre 1}
\end{figure} \par
\begin{figure}[!ht]\centering
\includegraphics[scale=0.6]{template/Image/capcom1.png}
\caption{Module de la transmittance du ﬁltre 2}
\end{figure} \newpage
\section{Mesures}
\subsection{Filtrage passe-bas à capacités commutées de type actif}Nous saisons que la fréquence d'horloge $f_2$ détermine la fréquence de coupure $f_c$ de ces filtres : soit $f2/75=f_{c1}$ pour le LTC1064-3(filtre n°1 de type Bessel, à déphasage proportionnel à la fréquence dans la bande passante), soit $f2/50=f_{c2}$ pour le LTC1064-4(filtre n°2 de type Cauer ou elliptique, à coupure très raide d'environ 80[dB/octave]). La maquette doit être alimentée en +5[V] et en ±15[V]. 
\paragraph{3.1.1}Utiliser l'horloge interne de la maquette ou si elle n'est pas disponible la sortie TTL d'un générateur de fonction et vérifier la relation entre la fréquence de coupure et la fréquence de l'horloge pour le filtre LTC1064-3(n°1) en mettant à l'entrée "filtre" une sinusoïde de 2 ou 3volts d'amplitude maximale et en recherchant la fréquence pour laquelle l'atténuation est de 3[dB](soit 5/7ème) pour diverses fréquences d'horloge dans la gamme 75[kHz]–5[MHz].\newline
\textbf{Réponse} : L'horloge interne ne fonctionne pas, alors nous avons utilisé l'horloge externe : un signal carré d'amplitude de 5[V] avec l'offset de 2.5[V] ; et nous avons défini les paramètres du signal entrée sinusoïde à : amplitude=2[V], phase=0, offset=0, fréquence $f_{c1}$ varie(voir le tableau ci-dessous), également pour toute la suite. 
\begin{center}
    \begin{tabular}{|c|c|c|c|c|c|c|c|c|}
    \hline
    $f_2[Hz]$ &5M &4M &3M &2M &1M &500K &100K &75K\\\hline
    $f_{c1}[kHz]$ &66.67 &53.33 &40.00 &26.67 &13.33 &6.67 &1.33 &1.00\\\hline
    $S_1[V]$ &1.56 &1.5 &1.48 &1.44 &1.46 &2 &1.42 &1.42\\\hline
    $E[V]$ &2.12 &2.12 &2.12 &2.08 &2.08 &2.08 &2.04 &2.04\\\hline
    $S_1/E$ &0.74 &0.71 &0.70 &0.69 &0.70 &0.96 &0.70 &0.70\\
    \hline
    \end{tabular}
\end{center}\par
 Nous avons réglé la $f_2$ à 8 valeurs différentes entre la gamme 75[kHz]–5[MHz], nous avons calculé et appliqué les valeurs de $f_{c1}=f2/75$ correspondant, puis nous visualisions les amplitudes du signal sortie(S[V]) et les amplitude du signal entrée(E[V]) sur l'oscilloscope. Nous savons que $$Attenuation=3[dB]=20log(|\frac{S}{E}|)$$ alors $$\Rightarrow \frac{S}{E}=\frac{5}{7}=0.71$$ donc pour tous les $S_1/E$ au tour de 0.71(sauf $f_2=500[KHz]$), la relation $f_2/75=f_{c1}$ est vérifiée.
\paragraph{3.1.2}Pour une fréquence de coupure du filtre de quelques kHz(à choisir à l'aide de la fréquence d'horloge), observer la FFT du signal de sortie lorsque la fréquence de la sinusoïde à l'entrée varie de part et d'autre de la fréquence de coupure et en déduire le tracé du module de la transmittance en fonction de $f_2/f_{c1}$. Comparer avec la préparation.\newline
\textbf{Réponse} : La question précédente nous a permit de pouvoir délimité notre plage de fréquence de coupure en fonction de la fréquence de l'horloge par la relation $\frac{f_2}{75}$ ( avec f2 la fréquence de notre horloge) et c'est ce qui nous permet de pouvoir choisir sans trop de réflection une fréquence de coupure pour l'étude du FFT.\newline
La fréquence de coupure $f{c1}=1[kHz]$ qui à pour fréquence d'horloge 75kHz est celle que nous choisissons pour observer la FFT de notre signal en parcourant ces alentours pour en déduire le module de sa transmittance.\newline
\begin{figure}[!ht]\centering
\includegraphics[scale=0.15]{template/Image/IMG_9709.jpg}
\caption{FFT du signal d'entrée de la $f_{c1}$}
\end{figure} \newline
\begin{figure}[!ht]\centering
\includegraphics[scale=0.18]{template/Image/IMG_9710.jpg}
\caption{FFT du signal de sortie de la $f_{c1}$}
\end{figure} \newpage
\begin{figure}[!ht]\centering
\includegraphics[scale=0.8]{template/Image/module1.png}
\caption{module de la transmittance}
\end{figure}\par
Observation : Nous remarquons après nos différents mesure que Les résultats obtenu sont en accord avec les résultats de la préparation.
\paragraph{3.1.3}Pour une fréquence de coupure de quelques kHz, et toujours un signal sinusoïdal à l'entrée, mesurer le déphasage entre la sortie et l'entrée pour 5 ou 6 fréquences équidistantes entre zéro et la fréquence de coupure du filtre. Tracer ce déphasage $\Phi$ en fonction de la fréquence en échelle linéaire. Est-ce que le filtre mérite le qualificatif de "filtre linéaire"(sous-entendu apportant un déphasage variant linéairement avec la fréquence dans la bande passante)? Déduire de la courbe le temps moyen de propagation de groupe $-d\phi/d\omega=-d\phi/(2\pi df)$.\newline
\textbf{Réponse} : Pour le filtre LTC1064-3(n$^\circ1$), nous avons défini la fréquence d'horloge externe $f_{c1}=75[kHz]$, donc la fréquence de coupure du signal entrée$f_c=1[kHz]$. Et nous avons obtenu le déphasage $\phi$ en [$\mu$s], puis nous pouvons le déphasage $\phi$ en radian(voir le tableau ci-dessous),  d'après la formule $$\phi[rad]=2\pi\frac{\phi[s]}{T}$$, avec T la période du signal. Nous avons la période $T=\frac{1}{f}=\frac{1}{1k}=1[ms]$.
\begin{center}
    \begin{tabular}{|c|c|c|c|c|c|c|}
    \hline
    $f_2[kHz]$ &0 &15 &30 &45 &60 &75\\\hline
    $f_{c1}[Hz]$ &0 &200 &400 &600 &800 &1k\\\hline
    $\phi[\mu s]$ &/ &2400 &1240 &800 &620 &500\\\hline
    $\phi[rad]$ &/ &15.08 &7.79 &5.03 &3.90 &3.14\\\hline
    $\phi[rad]$ &/ &$4.8\pi$ &$2.48\pi$ &$1.6\pi$ &$1.24\pi$ &$\pi$\\
    \hline
    \end{tabular}
\end{center}
Nous avons tracé la diagramme de déphasage $\phi$ en fonction de la fréquence de coupure suivante :
\begin{figure}[!ht]\centering
\includegraphics[scale=0.6]{template/Image/phi(fc1).png}
\caption{Diagramme de $\phi(fc1)$}
\end{figure}\newline
Nous pouvons dire que le filtre ne mérite pas de qualificatif de "filtre linéaire", parce que la courbe n'est pas linéaire. Ensuite nous déduirons la courbe le temps moyen de propagation de groupe $d\phi/d\omega=d\phi/(2\pi df)$.
\paragraph{3.1.4}Vérifier la relation entre la fréquence de coupure et la fréquence de l'horloge pour le filtre LTC1064-4(n°2) en mettant à l'entrée "filtre" une sinusoïde de 2 ou 3 volts d'amplitude maximale et en recherchant la fréquence pour laquelle l'atténuation est de 3[dB](soit 5/7ème) pour diverses fréquences d'horloge dans la gamme 75kHz–5MHz.\newline
\textbf{Réponse} : En appliquant les différentes fréquences d'horloge externe $f_2$ et de signal entrée $f_{c2}=f_2/50$, nous avons visualisé les amplitudes de signal sortie($S_2[V]$) et de signal entrée(E[V]), présentés dans le tableau ci-dessous.
\begin{center}
    \begin{tabular}{|c|c|c|c|c|c|c|c|c|}
    \hline
    $f_2[Hz]$ &5M &4M &3M &2M &1M &500K &100K &75K\\\hline
    $f_{c2}[kHz]$ &100 &80 &60 &40 &20 &10 &2 &1.5\\\hline
    $S_2[V]$ &1.38 &1.36 &1.34 &1.3 &2.24 &1.88 &1.84 &1.84\\\hline
    $E[V]$ &2.12 &2.12 &2.12 &2.08 &2.08 &2.08 &2.04 &2.04\\\hline
    $S_2/E$ &0.65 &0.64 &0.63 &0.63 &1.08 &0.90 &0.90 &0.90\\\hline
    $S_2/2$ &0.69 &0.68 &0.67 &0.65 &1.12 &0.94 &0.92 &0.92\\
    \hline
    \end{tabular}
\end{center}\par 
Nous avons constaté que l'amplitude E dépasse un peu de 2[V], c'est l'erreur normale, nous pouvons calculer également $S_2/2$. Avec les même formules dans la question 3.1.1, nous pouvons dire que pour les fréquences $f_2$ de 5[MHz] à 2[MHz], c'est-à-dire $f_{c2}$ de 100[KHz] à 40[KHz], la relation $f_{c2}=f_2/50$ est vérifiée. Si nous voulons garder l'atténuation de 3[dB], nous perdrons un peu de fréquence de coupure du signal entrée. 
\paragraph{3.1.5}Pour une fréquence de coupure du filtre de quelques kHz, observer la FFT du signal de sortie lorsqu'on la fréquence de la sinusoïde à l'entrée varie de part et d'autre de la fréquence de coupure et en déduire le tracé du module de la transmittance en fonction de $f/f_c$ en échelle linéaire. Comparer avec la préparation.\newline
\textbf{Réponse} : La fréquence de coupure $fc= 30kHz$ qui à pour fréquence d'horloge 1.5MHz est celle que nous choisissons pour observer la FFT de notre signal en parcourant ces alentours pour en déduire le module de sa transmittance.\newline
\begin{figure}[!ht]\centering
\includegraphics[scale=0.15]{template/Image/IMG_9707.jpg}
\caption{FFT du signal d'entrée de la $f_c$ }
\end{figure} \newline
\begin{figure}[!ht]\centering
\includegraphics[scale=0.15]{template/Image/IMG_9706.jpg}
\caption{FFT du signal de sort1e de la $f_c$}
\end{figure} \newpage
\begin{figure}[!ht]\centering
\includegraphics[scale=0.8]{template/Image/module2.png}
\caption{ module de la transmittance}
\end{figure}\par
Observation : Nous remarquons après nos différentes mesure que les résultats obtenus sont similaire par rapport aux résultats de la préparation.\newpage
\paragraph{3.1.6}Connecter un signal carré à l'entrée filtre et faire varier la fréquence du filtre pour éliminer progressivement les harmoniques de ce signal en observant alternativement le signal temporel et sa FFT à la sortie du filtre pour chaque fréquence de coupure du filtre n°2. Quelle relation peut-on mettre en évidence qualitativement entre la pente maximale du signal temporel et son spectre ?\newline
\textbf{Réponse} : Nous avons réglé le signal entrée à un signal carré comme ci-dessus :
\begin{figure}[!ht]\centering
\includegraphics[scale=0.09]{template/Image/Signal3.6.jpg}
\caption{Signal entrée de 3.1.6}
\end{figure}\newline
Alors la fréquence d'horloge est 1.5[MHz], nous avons obtenu la FFT de signal sortie suivante(voir la Figure 12). Nous voyons bien le premier spectre est le signal sortie de la fréquence de 30[kHz], et deux harmoniques de 60[kHz] et 90[kHz].
\begin{figure}[!ht]\centering
\includegraphics[scale=0.15]{template/Image/FFT3.1.6.jpg}
\caption{FFT de 3.1.6}
\end{figure}\newpage
\section{Conclusion}
A la ﬁn de ce travail pratique, nous avons puis apprendre qu'il existait une relation linéaire entre la fréquence de l'horloge et la fréquence de coupure du filtre. Nous avons aussi remarqué sur ces filtres que le déphasage est en exponentiel à la fréquence des signaux (d’entrée et de sortie). Il est aussi indépendant de la fréquence de coupure. Ces deux ﬁltres sont un peu particuliers car leurs fréquences de coupures sont réglables en jouant sur la période de l’horloge, à coté de cela, chacun de ces ﬁltres constitue un ﬁltre et un échantillonneur en même temps. Les deux ﬁltrés étudiés dans le TP sont des filtres passe-bas .
\end{document}

